//
//  ArticleTableViewCell.swift
//  NewsApp
//
//  Created by Jansen Ducusin on 2/3/21.
//

import Foundation
import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var descriptionLabel:UILabel!
    
}

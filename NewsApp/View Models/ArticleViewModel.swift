//
//  ArticleViewModel.swift
//  NewsApp
//
//  Created by Jansen Ducusin on 2/3/21.
//

import Foundation

struct ArticleViewModel{
    private let article: Article
}

extension ArticleViewModel{
    init(_ article: Article) {
        self.article = article
    }
}

extension ArticleViewModel{
    var title:  String {
        return self.article.title ?? "Unamed Article"
    }
    
    var description:    String{
        return  self.article.description ?? "No Description"
    }
}

//
//  Article.swift
//  NewsApp
//
//  Created by Jansen Ducusin on 2/3/21.
//

import Foundation

struct ArticleList: Decodable{
    var articles:   [Article]?
}

struct Article: Decodable{
    var author: String?
    var title:  String?
    var description:    String?
}

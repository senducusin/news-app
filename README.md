**News App Using MVVM**

This is a sample app that utilizes a News API, UIKit, Swift. The source code, demonstrates a simple implementation of MVVM structure in Swift language.

---

## API

*The API used is from https://newsapi.org/*

---

## Features
- Display current tech news in a list format